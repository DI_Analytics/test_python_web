# Test web python
## Before starting
 * Fork the project in your namespace
 * **Caution! very important:** Change project visibility: 
   * Click on `Settings` bottom left, when you see your project 
   * Select `General`
   * Click on expend `Visibility, project features, permissions` 
   * Change project visibility from public to private
## After pushing your codes
 * Add it@dataimpact.fr as **reporter** in `members`
 * Answer the email with your project link in it 
 
# Test
Some really important tips:
 * Comments and automated tests are a big plus
 * You might need pytest (and you can use any other package)
 * There are three parts (algorithm, advanced, web) you can choose to skip algorithm or advanced.
 * You can use internet
 

## First Part (python algorithm)

### Exercise 1
(I repeat myself I will not correct your test if you don't change visibility of the project from public to private, so please start with this)


In the function exercise_one on first_part.src module:
print every number between 1 and 100 as follows:
 * For every multiple of 3 print "Three".
 * For every multiple of 5 print "Five".
 * And for every multiple of both 3 and 5 print "ThreeFive"

*The output should be as follows:*

```
1
2
Three
4
Five
Three
7
8
Three
Five
11
Three
13
14
ThreeFive
16
```

### Exercise 2 (15 min)
Determine whether a positive integer number is colorful or not.

263 is a colorful number because [2, 6, 3, 2x6, 6x3, 2x6x3] are all different; whereas 236 is not colorful, because [2, 3, 6, 2x3, 3x6, 2x3x6] have 6 twice.

So take all consecutive subsets of digits, take their product and ensure all the products are different.
Examples:
```
263  -->  true
236  -->  false
2532 -->  false
```

### Exercise 3 (10 min)

Write a function calculate that takes a list of strings a returns the sum of the list items that represents an integer (skipping the other items).
Examples
```
calculate(['4', '3', '-2']) ➞ 5
calculate(453) ➞ False
calculate(['nothing', 3, '8', 2, '1']) ➞ 9
calculate('54') ➞ False
```

### Exercise 4 (25 min)
Write a function that will find all the anagrams of a word from a list. You will be given two inputs a word and an array with words. You should return an array of all the anagrams or an empty array if there are none.

Examples:

```
anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) => ['aabb', 'bbaa']

anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']) => ['carer', 'racer']

anagrams('laser', ['lazing', 'lazy',  'lacer']) => []
```

## Second Part (python advanced)

### Exercise 1 (5 min)

Create a generator named random_gen that generates random numbers (use random module) between 10 and 20 and stops just after giving 15

### Exercise 2 (10 min)

Rewrite decorator_to_str to force the functions "add" and "get_info" to return string values 

### Exercise 3 (10 min)

Rewrite `ignore_exception` so that it ignores the exception in its argument and returns None if this exception raises

### Exercise 4 (20 min)

Write the tests for the class `CacheDecorator` without touching it, some of your tests should not pass because this class is a little buggy. 

### Exercise 5 (10 min)

Write the metaclass `MetaInherList` so that the class `ForceToList` inherits from `list` built-in. (read `test_meta_list` in the tests for more information)

### Exercise 6 (15 min)
Create a metaclass that checks if classes have an attribute named 'process' which must be a method taking 3 arguments

## Third Part (django)

### Exercise 1 (20 min)
In the 'django_exercice_1' module, we have a mini-project django with a database with the following architecture:
  * There are multiples ships with different classes and statuses.
  * Each ship has one captain but can have multiple officers.
  * An officer can have multiple ranks.

Using the django migrations system, create a sqlite3 database containing the architecture above(the models are 
already created in models.py), and upload the fixtures in the database.

Next, you need to write some views(one view for each case) that return the following elements in json format:
  * A list of all the ships and its captain, in the following format:
```
{'ships': [{'name': 'ship 1', 'captain': 'James'},
           {'name': 'ship 2', 'captain': 'Oliver'} ...]}
```
  * A list of all the officers in the ship called 'Ship 2' in the following format:
```
{'officers': [{'name': 'Henry', 'order': 1},
              {'name': 'Lucas', 'order': 2} ...]}
```
  * A list of all the officers that have the rank 'Commander' in their ranks in the following format:
```
{'officers': [{'name': 'Henry', 'order': 1, ranks: ['Admiral', 'Commander', 'Ensign']},
              {'name': 'Lucas', 'order': 2, ranks: ['Lieutenant', 'Commander']},
              {'name': 'Alex', 'order': 3, ranks: ['Commander']} ...]}
```

### Exercise 2 (35 min)
The objective is to build a user access management system for a website application that has multiples tabs, 
where the end users belong to companies that have bought access rights to certain tabs of the web application.

Using Django models, create an sqlite3 database with the following architecture:
  * A table containing multiple users, each user belongs to a company
  * The company(the client) has a contract on the tool in which they have access only to certain tabs
  * We want the to be able to personnalize the access to the tabs per user. The user can have access to all the tabs 
    in the contract or less.

After creating the sqlite Database with the previous architecture, create a view that returns the info of the user and 
the tabs that he has access to in the tool in the following format:
```
{'first_name': 'Sara',
 'Last_name': 'Smith',
 'email': 'sara.smith@nestle.fr',
 'company': 'Nestle FR',
  'tabs': [{'id': 1, 'name': 'tab 1'},
           {'id': 2, 'name': 'tab 2'} ...]
```
