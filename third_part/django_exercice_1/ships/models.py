from django.db import models


class Officer(models.Model):
    order = models.IntegerField()
    name = models.CharField(max_length=256)
    rank = models.ManyToManyField('Rank')
    ship_assignment = models.ForeignKey('Ship', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name


class Captain(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Rank(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Ship(models.Model):
    name = models.CharField(max_length=256)
    ship_class = models.CharField(max_length=256)
    status = models.CharField(max_length=256, choices=(('hold', 'On Hold'),
                                                       ('deployed', 'Deployed'),
                                                       ('maintenance', 'In Maintenance')))
    captain = models.ForeignKey(Captain, models.CASCADE)

    def __str__(self):
        return self.name
